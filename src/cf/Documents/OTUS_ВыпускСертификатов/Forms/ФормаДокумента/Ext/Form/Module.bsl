﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Ключ.Пустая() Тогда 
		
		Если Параметры.Свойство("Основание") и ТипЗнч(Параметры.Основание) = Тип("ДокументСсылка.OTUS_ЗаявкаНаЭмиссиюСертификатов") Тогда
			
			СтатусЗаявки = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Параметры.Основание, "Статус");
			
			Если не СтатусЗаявки = Перечисления.OTUS_СтатусыЗаявокНаЭмиссиюПодарочныхСертификатов.Согласовано Тогда
				
				ОбщегоНазначения.СообщитьПользователю("Ввод на основании запрещен!",,,, Отказ);
				СтандартнаяОбработка = Ложь;
				Возврат;
				
			КонецЕсли;
			
		КонецЕсли;
		
		Объект.ДатаВыпуска = ТекущаяДатаСеанса();
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыВидыСертификатов

&НаКлиенте
Процедура ВидыСертификатовПриАктивизацииСтроки(Элемент)
	
	ПодключитьОбработчикОжидания("ВидыСертификатовПриАктивизацииСтрокиЗавершение", 0.1, Истина);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСертификаты

&НаКлиенте
Процедура СертификатыПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа, Параметр)
	
	Отказ = Истина;
	
КонецПроцедуры

&НаКлиенте
Процедура СертификатыПередУдалением(Элемент, Отказ)
	
	Отказ = Истина;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура КомандаОбработать(Команда)
	
	ОписаниеОповещения = Новый ОписаниеОповещения("КомандаОбработатьЗавершение", ЭтотОбъект);
	
	ПоказатьВопрос(ОписаниеОповещения, "Будут созданы сертификаты, продолжить?", РежимДиалогаВопрос.ДаНетОтмена,, КодВозвратаДиалога.Отмена);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура КомандаОбработатьЗавершение(Результат, Параметры) Экспорт
	
	Если не Результат = КодВозвратаДиалога.Да Тогда
        Возврат;
    КонецЕсли;

	СоздатьСертификатыНаСервере();

КонецПроцедуры

&НаКлиенте
Процедура ВидыСертификатовПриАктивизацииСтрокиЗавершение()

	ТекущиеДанные = Элементы.ВидыСертификатов.ТекущиеДанные;
	
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
    Если ТекущиеДанные = Неопределено Тогда
        Элементы.Сертификаты.ОтборСтрок = Неопределено;
    Иначе
        Элементы.Сертификаты.ОтборСтрок = Новый ФиксированнаяСтруктура("ИдентификаторСтроки", ТекущиеДанные.ИдентификаторСтроки);
    КонецЕсли;	
	
КонецПроцедуры

&НаСервере
Процедура СоздатьСертификатыНаСервере()
	
	НачатьТранзакцию();
	
	Отбор = Новый Структура("ИдентификаторСтроки");
	
	Для Каждого Строка Из Объект.ВидыСертификатов Цикл
		
		НайденныеСтроки = Объект.Сертификаты.НайтиСтроки(Отбор);
		
		КоличествоОбъектов = Строка.Количество - НайденныеСтроки.Количество();
		
		Для Итератор = 1 По КоличествоОбъектов Цикл
			
			НовыйСертификат = СоздатьСертификат(Строка.ВидСертификата, Строка.СрокДействия, Объект.ДатаВыпуска);
			
			НоваяСтрока = Объект.Сертификаты.Добавить();
			НоваяСтрока.Сертификат = НовыйСертификат;
			НоваяСтрока.ИдентификаторСтроки = Строка.ИдентификаторСтроки;
			
		КонецЦикла;
		
	КонецЦикла;
	
	ЗафиксироватьТранзакцию();
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция СоздатьСертификат(ВидСертификата, СрокДействия, ДатаВыпуска)
	
	НовыйОбъект = Справочники.OTUS_ПодарочныеСертификаты.СоздатьЭлемент();
	НовыйОбъект.ВидСертификата = ВидСертификата;
	НовыйОбъект.СрокДействия = СрокДействия;
	НовыйОбъект.Записать();
	
	Возврат НовыйОбъект.Ссылка;
	
КонецФункции

#КонецОбласти